# Aerodynamic modelling
![Screenshot.png](/examples/Screenshot.png)

## General Workflow

This is intended to be used in combination with the sail making FreeCAD macros in the sister project. So it is intended that a rough wing shape be generated in FreeCAD and the lists of numbers for chords, spans, dihedrals etc be cut and pasted from the Configurations spreadsheet into the Wing Gen iPython notebook. This notebook is then used to generate an OpenVSP file. This file can then be used for analysis, such as the Vortex Lattice Model results shown below. If it is edited in OpenVSP then it should be saved and the Wing Gen notebook can be used to read this file to create the lists which can then be pasted back into the FreeCAD document.

## Current Workflow

The main purpose of this repo is to provide billow calcualtions and wing optimisation functions for the design of the next generations of hang glider wings. To this end, the main funtions of use will currently be found in the "AltVSPScratchpad.ipynb" notebook that is to be opened with JupyterLab/Notebooks.

There are three core datastructures that are used for all calculations:
- a "glider" dictionary for import, export and plotting purposes
- an "allPts" dictionary containing the lists of all op points used in calculations
- a "lodDict" dictionary containing all the aero outputs from OpenVSP

The main current usefull functions are:
- convergebillow
- wingopt (which is called through scipy.optimise.minimise(), note that this is still WIP)
The main plotting functions are:
- PlotGliderElevation - this takes a glider dict and plots the relevant lines
- PlotSpanwiseForces - this currently takes several parameters which need to be precalculated with their relevant functions
- PlotWingPlanPts - this currently takes several parameters which need to be precalculated with their relevant functions


## To do
- wing optimisation fix
- clean up plotting functions
- use more numpy for array calculation

## Dependencies


- FreeCAD and sail-making macros (not actually dependant but import and export glider dict)
- OpenVSP with the API installed
- Jupyter Lab or Notebook with the VSP kernel accessible
