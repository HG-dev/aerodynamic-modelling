import openvsp as vsp
import scipy
import csv
import numpy as np
import numpy.polynomial.polynomial as poly
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from CODE.basicgeo import P2, P3, interpP3
import ezdxf
from CODE.dxfgrouping import filterlayerelements, makemergevertset, linearizeelement
from CODE.dxfgrouping import getblockcomponent, dxfoutputblocks

##################################################################
#    Library originally written by Tim Swait
#    Additions and refactoring by Sean CT
##################################################################

##################################################################

# maxThick: maximum allowable thickness (aerofoils thicker than this scaled to achieve this), set as none to not scale thickness
#t_min: minimum actual thickness of aerofoil in mm, set as none to not scale thickness
#fanme: file name (inc full path) of file to use as base (blank file with default settings)
def generateWing(wingDict, fname, AFfolder, maxThick = None, t_min = None):
    vsp.ReadVSPFile(fname)
    spans = []
    chords =[]
    dSurfs =[]
    
    for i in range(1,len(wingDict['secs'])):
        spans.append((wingDict['secs'][i]-wingDict['secs'][i-1])/1000)
    
    for i in range(len(wingDict['chordsmm'])):
        chords.append(wingDict['chordsmm'][i]/1000)
        dSurfs.append(wingDict['ds'][i]/wingDict['chordsmm'][i])
    
    rootpts = np.loadtxt(wingDict['root_file'], delimiter = ',')
    midpts = np.loadtxt(wingDict['mid_file'], delimiter = ',')
    outbpts = np.loadtxt(wingDict['outb_file'], delimiter = ',')
    tippts = np.loadtxt(wingDict['tip_file'], delimiter = ',')
    secpts = [rootpts,midpts,outbpts,tippts]
    
    for i in range(len(wingDict['secs'])):
        pts=interpolate_section(wingDict['secs'][i]/wingDict['secs'][-1], secpts, wingDict['rs'])
        if t_min is not None:
            t=t_min/chordsmm[i]
            pts = convertDS(pts,dSurfs[i], 50/wingDict['chordsmm'][0], t)
        else:
            t = 0.001
            pts = convertDS(pts,dSurfs[i], 50/wingDict['chordsmm'][0], t)
        thick = findThickness(pts)
        #print('thickness',thick)
        if maxThick is not None and thick > maxThick:
            pts = scaleSection(pts, scaleTH = maxThick/thick, minthick = t)
        OPdatFile(pts, AFfolder+str(i)+'.dat') # !!!!NOTE: WILL NEED TO MODIFY OTHER FUNCTIONS THAT USE THIS TO SEND AFfolder as a string including the path!!!!
    
    wid = vsp.AddGeom( "WING", "" )
    vsp.SetGeomName( wid, "FreeCAD wing")
    
    for i in range(len(spans)):
        vsp.InsertXSec( wid, 1, vsp.XS_FILE_AIRFOIL)
        vsp.Update()
    
    
    
    vsp.CutXSec(wid, 1)
    xsec_surf = vsp.GetXSecSurf(wid, 0 )
    vsp.ChangeXSecShape(xsec_surf, 0, vsp.XS_FILE_AIRFOIL) # Change root section to AF file
    xsec = vsp.GetXSec(xsec_surf, 0)
    AFname = AFfolder+str(0)+'.dat'
    print(AFname)
    #vsp.ReadFileAirfoil(xsec, AFname)
    vsp.Update()
    
    vsp.SetDriverGroup( wid, 1, 1, 5, 6)
    
    xsec = vsp.GetXSec(xsec_surf, i)
    xsecstr = "XSec_0"
    vsp.SetParmVal(wid, "Twist",  xsecstr, wingDict['twists'][0]) # Set root incidence
    
    for i in range(len(spans)):
        xsec = vsp.GetXSec(xsec_surf, i)
        AFname = AFfolder+str(i)+'.dat'
        vsp.ReadFileAirfoil(xsec, AFname)
        xsecstr = "XSec_"+ str(i+1)
        vsp.SetParmVal(wid, "Root_Chord", xsecstr, chords[i])
        vsp.SetParmVal(wid, "Sweep", xsecstr, wingDict['sweeps'][i])
        vsp.SetParmVal(wid, "Sweep_Location", xsecstr, 0)
        vsp.SetParmVal(wid, "Span",  xsecstr, spans[i])
        vsp.SetParmVal(wid, "Dihedral",  xsecstr, wingDict['dihedrals'][i])
        vsp.SetParmVal(wid, "Twist",  xsecstr, wingDict['twists'][i+1])
        vsp.SetParmVal(wid, "Twist_Location",  xsecstr, 0)
        if i == len(spans)-1:
            vsp.SetParmVal( wid, "Tip_Chord", xsecstr, chords[-1])
            xsec = vsp.GetXSec(xsec_surf, i+1)
            AFname = AFfolder+str(i+1)+'.dat'
            vsp.ReadFileAirfoil(xsec, AFname)
        vsp.Update()
    S = vsp.GetParmVal( wid, "TotalArea", "WingGeom")
    print('Wing generated with', len(spans), 'points', 'Area = ',S)
def interpolate_section(r,secpts,rs):
	assert len(secpts) == len(rs), 'Must a position for every section'
	for pts in secpts:
		assert len(pts) == len(secpts[0]), 'sections must contain same number of points'
	OPpts = []
	for i in range(len(secpts[0])):
		ys = []
		for j in range(len(secpts)):
			assert secpts[j][i][0] == secpts[0][i][0], 'sections must have matching x ordinates'
			ys.append(secpts[j][i][1])
		y = np.interp(r, rs, ys)
		OPpts.append((secpts[0][i][0], y))
	return OPpts

def OPdatFile(pts, fname):
    assert type(pts[0]) == P2 , ('Points must be given as list of P2', type(pts[0]))
    upperpts = []
    lowerpts = []
    for i in range(len(pts)):
        if pts[i].u<pts[i-1].u:
            upperpts.insert(0,pts[i])
        else:
            lowerpts.append(pts[i])
    
    with open(fname,'w') as f:
        f.write('AF'+fname+' \n')
        f.write(str(len(upperpts))+' '+str(len(lowerpts))+'\n \n')
        for pt in upperpts:
            f.write(str(pt.u)+' '+str(pt.v)+'\n')
        for pt in lowerpts:
            f.write(str(pt.u)+' '+str(pt.v)+'\n')
            
#Note:
    #Ideally should correct bit around LE to match tube diameter
    #tol is the thickness of the single surface layer
def convertDS(pts,ds, seam,tol=0.01):
    l=len(pts)
    aerof=[]
    dsp = [ds,0]
    dsfp = [seam,0]
    found = False
    front = False
    foundSeam = False
    pl = pts[0]
    highlight=False
    sSurfpts =[]
	
    for p in pts:
        if ds >= 1: #What to do if 100% double surface
            return pts
        elif p[0] < ds and not found:
            dsp[1] = p[1] + ((ds-p[0])*(pl[1]-p[1]))/(pl[0]-p[0])
            found = True
            #print(dsp)
        if not found:
            sSurfpts.insert(0,P2(p[0],p[1]-tol))
            
        if not highlight and p[0]>pl[0]:
            highlight = True
            #print('found highlight',pl)
        
        if highlight and p[0] > seam:
            if not foundSeam:
                #dsfp[0] = p[0]
                #dsfp[1]= p[1] + ((seam-p[0])*(pl[1]-p[1]))/(pl[0]-p[0])
                dsfp = p
                #print('foundseam',dsfp)
                foundSeam = True
            
            if p[0]<dsp[0]:
                y = np.interp(p[0],[dsfp[0],dsp[0]],[dsfp[1],dsp[1]])
                aerof.append(P2(p[0],y)) 
            else:
                return aerof + sSurfpts
  
        else:
            aerof.append(P2(p[0],p[1]))
        pl = p
    return aerof

# scaleTH: Scale factor to scale thickness by
# minthick: minimum thickness at any point as ratio of chord
def scaleSection(sec, scaleTH, minthick = 0.001):
    upper = [P2(sec[0][0],sec[0][1])]
    lower = [P2(0,0)]
    #camber = []
    upper_scaled = []
    lower_scaled = []
    
    for i in range(1,len(sec)):
        if sec[i][0]<sec[i-1][0]:
            upper.append(P2(sec[i][0],sec[i][1]))
        else:
            lower.append(P2(sec[i][0],sec[i][1]))

    for pt in upper:
        ptL1 = lower[0]
        for i in range(len(lower)):
            ptL2 = lower[i]
            if ptL1.u<=pt.u<ptL2.u:
                break
            ptL1 = ptL2
        if ptL1.u == ptL2.u:
            r = 0
        else:
            r = (pt.u-ptL1.u) / (ptL2.u-ptL1.u)
        v = ptL2-ptL1
        ptL = ptL1 + v*r
        #print(ptL1,ptL,ptL2,r)
        ptC = pt - (pt-ptL)*0.5 #point on camber line
        #camber.append(ptC)
        ptUsc = P2((pt.u),(ptC.v + max(minthick/2, scaleTH*(pt.v-ptC.v))))
        ptLsc = P2((ptL.u),(ptC.v - max(minthick/2, scaleTH*(ptC.v-ptL.v))))
        upper_scaled.append(ptUsc)
        lower_scaled.insert(0,ptLsc)
        scaled = upper_scaled + lower_scaled
    return scaled

def findThickness(sec):
    upper = [P2(sec[0][0],sec[0][1])]
    lower = [P2(0,0)]
    thick = []
    
    for i in range(1,len(sec)):
        if sec[i][0]<sec[i-1][0]:
            upper.append(P2(sec[i][0],sec[i][1]))
        else:
            lower.append(P2(sec[i][0],sec[i][1]))

    for pt in upper:
        ptL1 = lower[0]
        for i in range(len(lower)):
            ptL2 = lower[i]
            if ptL1.u<=pt.u<ptL2.u:
                break
            ptL1 = ptL2
        if ptL1.u == ptL2.u:
            r = 0
        else:
            r = (pt.u-ptL1.u) / (ptL2.u-ptL1.u)
        v = ptL2-ptL1
        ptL = ptL1 + v*r
        thick.append((pt-ptL).Len())
        
    return max(thick)

def dxf2dat(dxfname, datname = None, dmax = 1e-1):
    d = ezdxf.readfile(dxfname)
    spline = filterlayerelements(d, 'SPLINE')
    vecs = linearizeelement(spline[0], dmax)
    AFpts = []
    for v in vecs:
        AFpts.append(P2(v[0],v[1]))
    #Find Leading and Trailing edge of profile:
    LEpt = P2(1,0)
    TEpt = P2(0,0)
    for pt in AFpts:
        if pt.u < LEpt.u:
            LEpt = pt
        if pt.u > TEpt.u:
            TEpt = pt
    #Translate profile to align to LEpt at origin:
    AFptsT = []
    sc = 1/(TEpt-LEpt).u
    for pt in AFpts:
        AFptsT.append((pt-LEpt) * sc)
    if datname:
        OPdatFile(AFptsT, datname)
    return AFptsT

##################################################################
#    Basic Aero calculations
##################################################################

# calculate the CG position from the lift equation and thin aerofoil theory
def trimCG(v, alphas, CLs, CMs, cref, Xref, Sref, L):
    CL_t = v2CL(v, Sref, L)
    f_CL_al = poly.polyfit(CLs, alphas,2)
    alpha_t = poly.polyval(CL_t, f_CL_al) #AoA at desired trim speed
    #print('alpha at desired trim speed:', round(alpha_t,1), 'degrees')
    f_CM_al = poly.polyfit(alphas,CMs,2)
    CMy_t = poly.polyval(alpha_t, f_CM_al) #CM at desired trim speed
    d = CMy_t * cref/CL_t
    return Xref-d

# calculate airspeed from lift equation
def CL2v(CL, Sref, L, rho = 1.225):
    q = L/(CL*Sref)
    v = np.sqrt(2*q/rho)
    return v

# calculate CL from lift equation
def v2CL(v, Sref, L, rho = 1.225):
    q = 0.5 * rho * v**2
    CL = L/(q*Sref)
    return CL
    
##################################################################
#    VSP Analysis
##################################################################

def analyseVLM(AoAStart, AoAEnd,AlphaNpts,Xref = None,VLM = True, Sref = None):
    analysis_name = "VSPAEROComputeGeometry"
    vsp.SetAnalysisInputDefaults(analysis_name)
    analysis_method = list(vsp.GetIntAnalysisInput(analysis_name, "AnalysisMethod" ))
    if VLM:
        analysis_method[0] = vsp.VORTEX_LATTICE
    else:
        analysis_method[0] = vsp.PANEL
    vsp.SetIntAnalysisInput(analysis_name, "AnalysisMethod", analysis_method)
    res_id = vsp.ExecAnalysis( analysis_name )
    analysis_name = "VSPAEROSweep"
    vsp.SetAnalysisInputDefaults(analysis_name)
    vsp.SetDoubleAnalysisInput(analysis_name, "AlphaStart", (AoAStart,), 0)
    vsp.SetDoubleAnalysisInput(analysis_name, "AlphaEnd", (AoAEnd,), 0)
    vsp.SetIntAnalysisInput(analysis_name, "AlphaNpts", (AlphaNpts,), 0)
    vsp.SetIntAnalysisInput(analysis_name, "NCPU", (16,), 0)
    if Xref is not None:
        vsp.SetDoubleAnalysisInput(analysis_name, "Xcg", (Xref,), 0)
    if Sref is not None:
        vsp.SetDoubleAnalysisInput(analysis_name, "Sref", (Sref,), 0)
    else:
        vsp.SetIntAnalysisInput(analysis_name, "RefFlag", (1,), 0)
    
    #vsp.SetIntAnalysisInput(analysis_name, "MachNpts", (1,), 0)
    vsp.Update()
    vsp.DeleteAllResults()
    res_id = vsp.ExecAnalysis(analysis_name)
    return res_id

# Get a dictionary of polar type results
def getResults():
    history_res = True
    res = {'CL':[],'CD':[],'L2D':[],'CMy':[],'AoA':[]}
    i=0
    while history_res:
        history_res = vsp.FindResultsID("VSPAERO_History",i)
        if history_res:
            res['CL'].append(vsp.GetDoubleResults(history_res, "CL", 0)[-1])
            res['CD'].append(vsp.GetDoubleResults(history_res, "CDtot", 0)[-1])
            res['L2D'].append(vsp.GetDoubleResults(history_res, "L/D", 0)[-1])
            res['CMy'].append(vsp.GetDoubleResults(history_res, "CMy", 0)[-1])
            res['AoA'].append(vsp.GetDoubleResults(history_res, "Alpha", 0)[-1])
        i+=1
    return res

# Get a dictionary of spanwise load distribution for a given run number
def getLod(runpt = 0):
    lod = vsp.FindResultsID("VSPAERO_Load",runpt)
    Xs = vsp.GetDoubleResults(lod, 'Xavg',0)
    Ys = vsp.GetDoubleResults(lod, 'Yavg',0)
    Zs = vsp.GetDoubleResults(lod, 'Zavg',0)
    #cls = vsp.GetDoubleResults(lod, 'cl',0)
    #cds = vsp.GetDoubleResults(lod, 'cd',0)
    #css = vsp.GetDoubleResults(lod, 'cs',0)
    #cds = vsp.GetDoubleResults(lod, 'cd',0)
    cxs = vsp.GetDoubleResults(lod, 'cx',0)
    cys = vsp.GetDoubleResults(lod, 'cy',0)
    czs = vsp.GetDoubleResults(lod, 'cz',0)
    cmxs = vsp.GetDoubleResults(lod, 'cmx*c/cref',0)
    cmys = vsp.GetDoubleResults(lod, 'cmy*c/cref',0)
    cmzs = vsp.GetDoubleResults(lod, 'cmz*c/cref',0)
    
    cents = [] #Centres of action of forces
    cfs = [] #Force coefficient vectors
    cms = [] #Moment coefficient vectors
    cps = [] #Locations of centres of pressure
    
    for x,y,z in zip(Xs,Ys,Zs):
        cents.append(P3(x,y,z))
    
    for x,y,z in zip(cxs,cys,czs):
        cfs.append(P3(x,y,z))
    
    for x,y,z in zip(cmxs,cmys,cmzs):
        cms.append(P3(x,y,z))
    
    for f,m,cent in zip(cfs,cms,cents):
        cps.append(Crz(f,m,cent.z))
    #Fix to use panel centres in y instead of calculated cp y values:
    for i in range(len(cps)):
        cps[i] = P3(cps[i].x, cents[i].y, cps[i].z)
    lodDict = {'cents': cents,
              'cfs': cfs,
              'cms':cms,
              'cps':cps}
    return lodDict




##################################################################
#    Calculating points as lists of P3 ; ALLpts is a dict of all these
##################################################################

# calculates ALL leading and trailing edge points
def calcLTEpts(secs,chordsmm,sweeps,dihedrals,twists):
    LEpts = calcLEpts(secs,sweeps,dihedrals)
    TEpts = calcTEpts(LEpts,chordsmm,twists)
    return LEpts, TEpts

def calcLEpts(secs,sweeps,dihedrals):
    LEpts = [P3(0,0,0)]
    for i in range(1,len(secs)):
        d = np.tan(np.radians(sweeps[i-1]))*(secs[i]/1000-secs[i-1]/1000)
        h = np.tan(np.radians(dihedrals[i-1]))*(secs[i]/1000-secs[i-1]/1000)
        LEpts.append(P3((LEpts[-1].x+d),(secs[i]/1000),(LEpts[-1].z+h)))
    return LEpts

# calculates ALL trailing edge points
def calcTEpts(LEpts,chordsmm,twists):
    TEpts = []
    for i in range(len(LEpts)):
        TEpts.append(calcTEpt(LEpts[i],chordsmm[i],twists[i]))
    return TEpts

# calculates ONE trailing edge point
def calcTEpt(LEpt,chordmm,twist):
    return P3(
            LEpt.x + (chordmm*np.cos(np.radians(twist))/1000),
            LEpt.y,
            LEpt.z - (chordmm*np.sin(np.radians(twist)))/1000)

def calcTLpts(LEpts,TEpts):
    #Vector of tension line from root trailing edge to tip leading edge
    vTL = LEpts[-1] - TEpts[0]
    
    #List of points along tension line
    TLpts = []
    for i in range(len(LEpts)):
        vChL = TEpts[i] - LEpts[i] #Vector of chord line
        r = LEpts[i].y / LEpts[-1].y - LEpts[0].y #ratio of distance along y axis
        x = TEpts[0].x + r*vTL.x
        y = LEpts[i].y
        rc = (x - LEpts[i].x)/(TEpts[i].x - LEpts[i].x)
        z = LEpts[i].z + rc*vChL.z
        TLpts.append(P3(x,y,z))
    return TLpts

def calcGliderpts(glider):
    LEpts,TEpts = calcLTEpts(glider['secs'],
                               glider['chordsmm'],
                               glider['sweeps'],
                               glider['dihedrals'],
                               glider['twists'])
    TLpts = calcTLpts(LEpts,TEpts)
    return LEpts, TEpts, TLpts

def calcAeropts(LEpts, TLpts, cps):
    aeroLEpts = interpP3(LEpts,cps,'y')
    aeroTLpts = interpP3(TLpts,cps,'y') # Points along tension line spaced at aerodynamic points
    return aeroLEpts, aeroTLpts

def calcALLpts(glider, lodDict):
    LEpts,TEpts,TLpts = calcGliderpts(glider)
    aeroLEpts,aeroTLpts = calcAeropts(LEpts, TLpts, lodDict['cps'])
    return({'LEpts':LEpts,'TEpts':TEpts,'TLpts':TLpts,'aeroLEpts':aeroLEpts,'aeroTLpts':aeroTLpts})

##################################################################
#    Calculating Twists
##################################################################
def calcTwists(LEpts,TLpts,tipchordmm):
    twists = []
    for i in range(len(LEpts)-1):
        twists.append(calcTwist(LEpts[i], TLpts[i]))
    twists.append(twists[-1]/2) #hotfix to get it working
    #twists[-1] = calclastbattentwist(LEpts, TLpts, tipchordmm,twists[-1]) #this is the real calculation
    return twists

def calcTwist(LEpt, TLpt):
    vLETL = TLpt - LEpt
    return -np.degrees(np.arctan(vLETL.z/vLETL.x))

#calculates using the penultimate LE point as docking for the batten on the LE
#replace allpts['LEpts'][-2] with another point to generalise
def calclastbattentwist(LEpts, TLpts, tipchordmm):
    X = TLpts[-2]-LEpts[-2]
    L = LEpts[-1]-LEpts[-2]
    lam = tipchordmm/(1000*X.Len())
    l = L*(1/(1+lam))
    x = X*0.5
    TLinter = interpP3(TLpts, [LEpts[-2]+l], xyz='y')[0]
    batten = TLinter-LEpts[-2]
    return -np.degrees(np.arctan(batten.z/batten.x))

##################################################################
#Calculates position of Cp in x and y for given z height when given force and moments
#f: P3 of force vector
#m: P3 of moment vector
#returns: P3 of Cp location
def Crz(f,m,zavg):
    # If r is the position of the force that generates the torque
    # then we need to find r = a*F + F x m where r_z = zavg
    # Subtituting w = r x F = (F x m) x F = (F.F)m - (F.m)F
    fsq = f.Lensq()
    cx, cy, cz = P3.Cross(f, m)
    cx, cy, cz = cx/fsq, cy/fsq, cz/fsq
    a = (cz - zavg)/f.z
    return P3(cx - a*f.x, cy - a*f.y, zavg)

##################################################################
#    Billow calculations ; P is the force distribution , Lratio reduces the TL length as if increasing the tension /!\ very sensitive!
##################################################################
def findBillow(TLpts, P, Lratio = 1, maxiters =1e3,tol = 1e-3, damper = 0.1/1000):
    #Work out length along tension line points
    length = 0
    for i in range(1,len(TLpts)):
        length += (TLpts[i]-TLpts[i-1]).Len()
    print('Tension line length', length)
    Ltarget = length * Lratio
    L_err = 1
    T_err = 1
    #tStart = time.time()
    iters=0
    zbase = []
    #vtot = TLpts[-1]-TLpts[0]
    for pt in TLpts:
        z = np.interp(pt.y, [TLpts[0].y,TLpts[-1].y], [TLpts[0].z,TLpts[-1].z])
        zbase.append(z)
    #print(zbase)
    
    while (L_err > tol or T_err > tol) and (iters < maxiters):
        iters +=1
        L = 0
        #Calc total length along curve
        for i in range(1,len(TLpts)):
            L += (TLpts[i]-TLpts[i-1]).Len()
        L_err = abs(Ltarget-L)/Ltarget
        
        #Calc tension at each point 
        Ts = []
        for i in range(1,len(TLpts)-1):
            v1 = TLpts[i]-TLpts[i-1]
            v2 = TLpts[i+1]-TLpts[i]
            ac = P3.Dot(v1,v2)/(v1.Len()*v2.Len())
            if ac > 1:
                a = 0
            else:
                a = np.arccos(ac)
            #print('angle',np.degrees(a))
            if P3.Cross(v1,v2).y < 0:
                #print('whoa')
                a = -a
            if a == 0:
                T = 0
            else:
                T = P[i-1]/(2*np.sin(a/2))
            #SIMPLIFIED USING TRIG IDENTITIES
            #c = P3.Dot(v1,v2)/(v1.Len()*v2.Len())
            #s = np.sqrt((1 - c)/2)
            #T = P[i] / (2*s)
            Ts.append(T)
            #print(np.degrees(a),T)
        T_av = np.mean(Ts)
        T_err = abs((max(Ts)-T_av)/T_av)
        #Adjust each point vertically by amount proportional to error in overall length and specific tension
        for i in range(1,len(TLpts)-1):
            if Ts[i-1] < 0:
                #print('whoa')
                v1 = TLpts[i]-TLpts[i-1]
                v2 = TLpts[i+1]-TLpts[i]
                zadj = v2.z - v1.z
            else:
                if TLpts[i].z > zbase[i]:
                    L_corr = (Ltarget-L)/Ltarget
                else:
                    L_corr = -(Ltarget-L)/Ltarget
                    
                if P[i-1] > 0:
                    T_corr = (Ts[i-1]-T_av)*damper
                else:
                    T_corr = -(Ts[i-1]-T_av)*damper
                zadj = max(-10/1000,(min(10/1000,(L_corr + T_corr))))
            #print('zadj',zadj)
            #print(TLpts[i])
            TLpts[i] = TLpts[i] + P3(0,0,zadj)
            #print(TLpts[i])
        
        #print(max(Ts),T_av)
    #print('Time elapsed', time.time()-tStart, 's. Iterations:',iters)
    print('Final length error',L_err, 'Final tension error', T_err, 'Iterations:',iters)
    if iters == maxiters:
        print('Maximum number of iterations reached')
    return TLpts


##################################################################
#    Calculate aero force components on the TL
##################################################################
    
def calcTLF(allpts,lodDict):
    # Find distance in x from LE to aero centres and to tension line
    Dcps, Dtls = [], []
    for i in range(len(allpts['aeroLEpts'])):
        Dcps.append(abs(lodDict['cps'][i].x-allpts['aeroLEpts'][i].x))
        Dtls.append(allpts['aeroTLpts'][i].x-allpts['aeroLEpts'][i].x)
    
    # Work out forces on tension line:
    tlfs = []
    for i in range (len(lodDict['cfs'])):
        tlfs.append(Dcps[i]*lodDict['cfs'][i].z/Dtls[i])
    
    TLFs = np.interp([pt.y for pt in allpts['TLpts']], [pt.y for pt in lodDict['cents']], tlfs)
    return TLFs, tlfs

##################################################################
#    Plotting
##################################################################

def PlotSpanwiseForces(allpts, lodDict, TLFs, tlfs):
    #plot spanwise lift and forces
    fig = px.line()
    fig.add_trace(go.Scatter(
        x=[pt.y for pt in lodDict['cents']], 
        y=[pt.z for pt in lodDict['cfs']], name = 'cz'
    ))
    fig.add_trace(go.Scatter(
        x=[pt.y for pt in lodDict['cents']], 
        y=[pt.x for pt in lodDict['cfs']], name = 'cx'
    ))
    fig.add_trace(go.Scatter(
        x=[pt.y for pt in lodDict['cents']], 
        y=[pt.y for pt in lodDict['cfs']], name = 'cy'
    ))
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in lodDict['cents']],
        y = tlfs, name = 'tlfs'
    ))
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in allpts['TLpts']],
        y = TLFs, name = 'TLFs'
    ))
    fig.show()

def PlotWingPlanPts(allpts, lodDict):
    #plot plan view of wing shape, tension line and centers of pressure
    fig = px.line()
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in allpts['TEpts']],
        y = [pt.x for pt in allpts['TEpts']], name = 'TE'
    ))
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in allpts['aeroLEpts']],
        y = [pt.x for pt in allpts['aeroLEpts']], name = 'aeroLE'
    ))
    fig.add_trace(go.Scatter(
        x=[pt.y for pt in allpts['LEpts']], 
        y=[pt.x for pt in allpts['LEpts']], name = 'LE'
    ))
    fig.add_trace(go.Scatter(
        x=[pt.y for pt in lodDict['cps']], 
        y=[pt.x for pt in lodDict['cps']], mode = 'markers', marker_symbol ='circle',name = 'cps'
    ))
    fig.add_trace(go.Scatter(
        x=[pt.y for pt in allpts['aeroTLpts']], 
        y=[pt.x for pt in allpts['aeroTLpts']], mode = 'markers', marker_symbol ='circle',name = 'TL'
    ))
    fig.show()
    
def PlotAllElevationPts(allpts, newTLpts, newTEpts):
    #plot elevation view of wing shape and tension line (old and new TL+TE)
    fig = px.line()
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in allpts['LEpts']],
        y = [pt.z for pt in allpts['LEpts']], name = 'LE'
    ))
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in allpts['TEpts']],
        y = [pt.z for pt in allpts['TEpts']], name = 'TE'
    ))
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in allpts['TLpts']],
        y = [pt.z for pt in allpts['TLpts']], name = 'TL'
    ))
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in allpts['aeroTLpts']],
        y = [pt.z for pt in allpts['aeroTLpts']], name = 'aeroTL'
    ))
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in newTLpts],
        y = [pt.z for pt in newTLpts], name = 'newTL'
    ))
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in newTEpts],
        y = [pt.z for pt in newTEpts], name = 'newTE'
    ))
    fig.show()

def PlotGliderElevation(glider):
    #plot elevation view of wing shape and tension line
    LEpts,TEpts,TLpts = calcGliderpts(glider)
    Plot_LETETL_Elevation(LEpts,TEpts,TLpts)

def Plot_LETETL_Elevation(LEpts,TEpts,TLpts):
    #plot elevation view of wing shape and tension line
    fig = px.line()
    fig.add_trace(go.Scatter(
        x=[pt.y for pt in LEpts], 
        y=[pt.z for pt in LEpts], name = 'LE'
    ))
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in TEpts],
        y = [pt.z for pt in TEpts], name = 'TE'
    ))
    fig.add_trace(go.Scatter(
        x = [pt.y for pt in TLpts],
        y = [pt.z for pt in TLpts], name = 'TL'
    ))
    fig.add_trace(go.Scatter(
        x = [LEpts[-2].y,TEpts[-1].y],
        y = [LEpts[-2].z,TEpts[-1].z], name = 'tip'
    ))
    fig.show()

##################################################################
#    Extra
##################################################################

def calcPRTpts(allpts):
    spacing = []
    npanels = len(allpts["aeroTLpts"])
    panellength0y = allpts["LEpts"][-1].y/npanels
    for i in range(npanels+1):
        spacing.append(P3(0,i*panellength0y,0))
    return interpP3(allpts["TLpts"], spacing, 'y')#list of particles' positions joining panels

# parameterising the chord distribution - chordratio = 1 for root = tipchord, 0 for tipchord = 0 - only works for an elliptical type planform
def chorddistribution(spacing, semispan, chordratio, area):
    c0 = area / (2*chordratio*semispan + np.pi*(1-chordratio)*semispan/2)
    chords = []
    for x in spacing:
        chords.append(c0*(chordratio+(1-chordratio)*np.sqrt(1-np.power(x/semispan,2))))
    return chords

def checksprogs(sprogsecs, sprogangles, twists, TLFs, stiffness = 0.1):
    for i in range(len(sprogsecs)):
        adelta = twists[sprogsecs[i]] - sprogangles[i] #get angle difference
        if adelta > 0: #if twist less than sprog
            TLFs[sprogsecs[i]] += adelta * stiffness #constant is a rotational stifness factor. arbitrary for now.
    return TLFs